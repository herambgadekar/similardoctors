package similar_doctors;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

public class SimilarDoctorsTest {

	@Test
	public void doctorListNullTest() {
		assertNull(SimilarDoctors.getDoctors(null, "ABC"));
	}

	@Test
	public void listAndToSearchBothNull() {
		assertNull(SimilarDoctors.getDoctors(null, null));
	}

	@Test
	public void nullSortList(){
		assertNull(SimilarDoctors.sortList(null));
	}

	@Test
	public void numericDocNameTest() {
		List<Doctor> allDoctors = new ArrayList<Doctor>();
		allDoctors.add(new Doctor("ABC", "Dentist", "San Francisco", 3.7));
		assertNull(SimilarDoctors.getDoctors(allDoctors, "123"));
	}

	@Test
	public void positiveScenario() {
		List<Doctor> allDoctors = new ArrayList<Doctor>();
		allDoctors.add(new Doctor("ABC", "Dentist", "San Francisco", 3.7));
		allDoctors.add(new Doctor("XYZ", "Dentist", "San Mateo", 2.7));
		assertEquals(allDoctors.get(1), SimilarDoctors.getDoctors(allDoctors, "ABC").get(0));
	}

	@Test
	public void toSearchDoctorNull() {
		List<Doctor> allDoctors = new ArrayList<Doctor>();
		allDoctors.add(new Doctor("ABC", "Dentist", "San Francisco", 3.7));
		assertNull(SimilarDoctors.getDoctors(allDoctors, null));
	}
}

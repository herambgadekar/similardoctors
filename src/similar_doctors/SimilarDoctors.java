package similar_doctors;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Similar doctor here means having the same speciality. The function sortList
 * (ArrayList<Doctor> toBeSortedList) returns all the doctors having the same
 * speciality prioritized according to their rating and then ordered
 * alphabetically according to the city.
 */
public class SimilarDoctors {

	public static void main(final String[] args) throws IOException {
		List<Doctor> allDoctors = new ArrayList<Doctor>();
		ArrayList<Doctor> desiredList;
		allDoctors.add(new Doctor("ABC","Dentist","San Francisco",3.7));
		allDoctors.add(new Doctor("DEF","Radiologist","Hayward",3.5));
		allDoctors.add(new Doctor("GHI","Pediatrician","Sunnyvale",4.2));
		allDoctors.add(new Doctor("JKL","Primary care physician","Mountain View",4.0));
		allDoctors.add(new Doctor("MNO","Cardiologist","San Mateo",3.8));
		allDoctors.add(new Doctor("PQR","Dentist","San Francisco",2.9));
		allDoctors.add(new Doctor("STU","Primary care physician","San Francisco",3.7));
		allDoctors.add(new Doctor("VWX","Cardiologist","San Mateo",4.1));
		allDoctors.add(new Doctor("YZA","Pediatrician","Sunnyvale",2.7));
		allDoctors.add(new Doctor("BCD","Cardiologist","San Mateo",3.2));
		allDoctors.add(new Doctor("EFG","Pediatrician","Sunnyvale",3.6));
		allDoctors.add(new Doctor("HIJ","Dentist","San Francisco",4.7));
		allDoctors.add(new Doctor("KLM","Radiologist","Hayward",4.1));
		allDoctors.add(new Doctor("NOP","Pediatrician","San Francisco",3.3));
		allDoctors.add(new Doctor("QRS","Primary care physician","Mountain View",3.4));
		allDoctors.add(new Doctor("TUV","Cardiologist","San Francisco",3.4));
		allDoctors.add(new Doctor("WXY","Dentist","Hayward",4.4));
		allDoctors.add(new Doctor("ZAB","Primary care physician","San Francisco",4.8));
		allDoctors.add(new Doctor("CDE","Primary care physician","Mountain View",3.9));
		allDoctors.add(new Doctor("FGH","Pediatrician","Hayward",3.5));
		allDoctors.add(new Doctor("IJK","Primary care physician","Sunnyvale",3.6));
		allDoctors.add(new Doctor("LMN","Pediatrician","San Mateo",2.9));

		System.out.println("Enter Doctors Name :");
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		String inputDoc = br.readLine();
		desiredList = sortList(getDoctors(allDoctors, inputDoc));

		for(Doctor doc:desiredList) {
			System.out.println(doc);
		}
	}

	static ArrayList<Doctor> getDoctors(final List<Doctor> allDoctors, final String doctorName) {
		int i = 0;
		if (doctorName != null && allDoctors != null) {
		ArrayList<Doctor> docList = new ArrayList<Doctor>();
		String nameToSearch = "";
		String ExpectedSpeciality = "";
		while (!nameToSearch.equalsIgnoreCase(doctorName) && i < allDoctors.size()) {
			nameToSearch = allDoctors.get(i).getName();
			i++;
		}
			if (i == allDoctors.size()) {
			System.out.println("Doctor with this name not found!");
			return null;
		}
		ExpectedSpeciality = allDoctors.get(i).getSpeciality();
		for(Doctor doc : allDoctors){
			if (doc.getSpeciality().equalsIgnoreCase(ExpectedSpeciality) && !doc.getName().equalsIgnoreCase(doctorName)) {
				docList.add(doc);
			}
		}
		return docList;
		}
		return null;
	}

	static ArrayList<Doctor> sortList(final ArrayList<Doctor> toBeSortedList) {
		if (toBeSortedList != null) {
			Collections.sort(toBeSortedList, new DoctorComparator(new RatingComparator(), new AreaComparator()));
		}
		return toBeSortedList;
	}
}

package similar_doctors;

import java.util.Arrays;
import java.util.Comparator;
import java.util.List;

public class DoctorComparator implements Comparator<Doctor>{

	List<Comparator<Doctor>> chainComparator;

	@SafeVarargs
	public DoctorComparator(final Comparator<Doctor>... comparators) {
		this.chainComparator = Arrays.asList(comparators);
	}

	@Override
	public int compare(final Doctor doc1, final Doctor doc2) {
		if (doc1 != null && doc2 != null) {
			for (Comparator<Doctor> comparator : chainComparator) {
				int result = comparator.compare(doc1, doc2);
				if (result != 0) {
					return result;
				}
			}
		}
		return 0;
	}
}

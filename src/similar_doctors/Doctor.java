package similar_doctors;

public class Doctor {

	private final String name;
	private final String speciality;
	private final String area;
	private final double rating;

	public Doctor(final String name, final String speciality, final String area, final double rating) {
		this.name = name;
		this.speciality = speciality;
		this.area = area;
		this.rating = rating;
	}

	public String getArea() {
		return area;
	}

	public String getName() {
		return name;
	}

	public double getRating() {
		return rating;
	}

	public String getSpeciality() {
		return speciality;
	}

	@Override
	public String toString() {
		return String.format("%s\t%s\t%s\t%.1f", name, speciality, area, rating);
	}
}
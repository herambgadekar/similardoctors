package similar_doctors;

import java.util.Comparator;

public class RatingComparator implements Comparator<Doctor>{

	@Override
	public int compare(final Doctor doc1, final Doctor doc2) {
		if(doc1!=null && doc2!=null) {
			return (doc2.getRating() > doc1.getRating() ? 1 : -1);
		}
		System.out.println("Either or both of the doctor objects are null");
		return 0;
	}
}

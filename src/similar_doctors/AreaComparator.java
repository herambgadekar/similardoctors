package similar_doctors;

import java.util.Comparator;

public class AreaComparator implements Comparator<Doctor> {

	@Override
	public int compare(final Doctor doc1, final Doctor doc2) {
		if(doc1!=null && doc2!=null) {
			return (doc1.getArea().compareTo(doc2.getArea()));
		}
		System.out.println("Either or both of the doctor objects are null");
		return 0;
	}
}
